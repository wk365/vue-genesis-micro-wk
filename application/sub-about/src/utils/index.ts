// interface availableValue {
//     value: object | [] | number | string
// }
// import { store } from '@/store/';

const util = {
    getNewObj: (obj: object | []) => {
        return JSON.parse(JSON.stringify(obj));
    },
    // storage存储，要判断只能在浏览器端使用
    storage: {
        get: (key: string) => {
            const value: any = window.localStorage.getItem(key);
            return (process as any).browser && window.JSON.parse(value);
        },
        set: (key: string, value: object | [] | number | string) => {
            (process as any).browser &&
                window.localStorage.setItem(key, window.JSON.stringify(value));
        },
        remove: (key: string) => {
            window.localStorage.removeItem(key);
        }
    }
};
export default util;
