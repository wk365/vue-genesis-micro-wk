import Vue from 'vue';
import Meta from 'vue-meta';
import ElementUI from 'element-ui';

// 国际化
// import VueI18n from 'vue-i18n';
// import locale from 'element-ui/lib/locale/';
// import {
//     i18nConfig
// } from "./utils/i18n";

import App from './app.vue';
import { createStore } from './store';
import { createRouter } from './router';
// import { createRequest } from './request';

// 拖动排序
// import VueDND from 'awe-dnd';
// Vue.use(VueDND)

Vue.use(Meta);
Vue.use(ElementUI);
// Vue.use(VueI18n);

// 默认语言
// const i18n = new VueI18n({
//     locale: 'zh', // 语言标识
//     messages: i18nConfig
// })
// locale.i18n((key, value) => i18n.t(key, value)) //重点：为了实现element插件的多语言切换

export { App, createStore, createRouter };
