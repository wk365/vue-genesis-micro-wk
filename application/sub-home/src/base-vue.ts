import Vue from 'vue';
import { State } from '@/store';

export class BaseVue extends Vue {
    // 获取 store 状态
    public get state() {
        return this.$store.state as State;
    }

    // 获取请求对象
    public get request() {
        return this.$root.$options.request!;
    }

    /**
     * element方法简单封装
     * 解决this定义时element的方法编辑器会报红
     */
    public showMessage(config: any) {
        return this.$message(config);
    }

    /**
     * 前后端通用请求方法
     */
    public async getData() {
        console.log('getData');
    }
}
