import Vue from 'vue';
import Vuex from 'vuex';
// import { Request } from '../request';

export interface BlogItem {
    author: string;
    content: string;
    id: number;
    createTime: number;
}

export interface State {
    site:string;
    skin: {},
    // user: {
    //     name: string;
    // };
    // userinfo: {};
    // banner: [],
    // home: {
    //     news: [],
    // };
    // blogList: BlogItem[];
}

Vue.use(Vuex);

/**
 * 提供一个工厂函数创建 store
 */

export const store = new Vuex.Store<State>({
    state: {
        site:'www.xxxx.com',
        skin: {},
    },
    mutations: {
        setSkin(state, data) {
            state.skin = data;
        },
        setSite(state, data) {
            state.site = data;
        },
        getBanner(state, data: []) {
            state.banner = data;
        },
        getHomeNewsList(state, data: []) {
            state.home.news = data;
        },
        //
        signin(state, name: string) {
            state.user.name = name;
        },
    },
    actions: {
        SETSITE: ({ commit }, data) => {
            commit('setSite', data)
        },
        SETSKIN: ({ commit }, data) => {
            commit('setSkin', data)
        }
    }
});
export const createStore = () => {
    return store;
};
