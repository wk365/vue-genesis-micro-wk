import { ClientOptions } from '@fmfe/genesis-core';
import { createClientApp } from '@fmfe/genesis-app';
import Vue from 'vue';
import { App, createStore, createRouter,createRequest } from './main';

/**
 * 客户端入口，需要导出一个方法，并且返回一个 Promise<Vue>
 */
export default async (clientOptions: ClientOptions): Promise<Vue> => {
    const request = createRequest();
    const store = createStore(request);
    // const store = createStore();
    const router = createRouter();

    /**
     * 把服务端下发的状态，还原到 store 中
     */

    clientOptions.state.vuexState && store.replaceState(clientOptions.state.vuexState);


    /**
     * 创建客户端应用程序
     */
    return createClientApp({
        /**
         * 根组件
         */
        App,
        /**
         * 客户端的选项
         */
        clientOptions,
        /**
         * new Vue({ ...vueOptions })
         */
        vueOptions: {
            store,
            request,
            router
        }
    });
};
