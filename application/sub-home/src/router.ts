// import { RouterMode } from 'vue-router';
import { Router } from '@fmfe/genesis-app';

// interface State {
//     routerMode?: RouterMode;
// }

export const createRouter = () => {
    return new Router({
        mode: 'history',
        routes: [
            {
                path: '/home/',
                component: () =>
                    import('./views/home.vue').then((m) => m.default)
            },
            {
                path: '/home/test',
                component: () =>
                    import('./views/test.vue').then((m) => m.default)
            }
        ]
    });
};
