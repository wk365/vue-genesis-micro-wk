import express from 'express';
import path from 'path';
import httpProxy from 'http-proxy';
import { SSR, Renderer } from '@fmfe/genesis-core';
import config from './config';
// import MyPlugin from './plug';

// let domain = process.env.NODE_ENV == 'production' ? 'http://192.168.254.117' : 'http://localhost';
const domain = config.domain;

/**
 * 创建一个应用程序
 */
export const app = express();

/**
 * 创建一个 SSR 实例
 */
export const ssr = new SSR({
    /**
     * 设置应用名称，不能重复
     */
    name: 'ssr-common',
    /**
     * 设置编译的配置
     */
    build: {
        /**
         * 设置项目的目录
         */
        baseDir: path.resolve(__dirname),
        template: path.resolve(__dirname, './index.html'),
        transpile: [
            path.resolve('./node_modules/@fmfe/genesis-app'),
            path.resolve('./node_modules/@fmfe/genesis-remote')
        ]
    }
});
// ssr.plugin.use(new MyPlugin(ssr));

/**
 * 拿到渲染器后，启动应用程序
 */
export const startApp = (renderer: Renderer) => {
    const proxy = httpProxy.createProxyServer({});
    /**
     * 注意：这里是为了演示，所以在聚合服务上代理了其它服务的请求，在生产环境中，你可能会使用 Nginx 进行反向代理。
     */
    /**
     * sub-home 服务静态文件代理
     */
    app.use('/sub-home/', (req, res, next) => {
        proxy.web(
            req,
            res,
            {
                target: `${domain}:3002/sub-home/`,
                changeOrigin: true
            },
            (err) => {
                res.status(500).send(err.message);
            }
        );
    });
    /**
     * sub-home api 代理
     */
    app.use('/api/sub-home/', (req, res, next) => {
        proxy.web(
            req,
            res,
            {
                target: `${domain}:3002/api/`,
                changeOrigin: true
            },
            (err) => {
                res.status(500).send(err.message);
            }
        );
    });
    /**
     * sub-about 静态资源代理
     */
    app.use('/sub-about/', (req, res, next) => {
        proxy.web(
            req,
            res,
            {
                target: `${domain}:3001/sub-about/`,
                changeOrigin: true
            },
            (err) => {
                res.status(500).send(err.message);
            }
        );
    });
    /**
     * sub-about api 代理
     */
    app.use('/api/sub-about', (req, res, next) => {
        proxy.web(
            req,
            res,
            {
                target: `${domain}:3001/api/`,
                changeOrigin: true
            },
            (err) => {
                res.status(500).send(err.message);
            }
        );
    });
    /**
     * 使用默认渲染中间件进行渲染，你也可以调用更加底层的 renderer.renderJson 和 renderer.renderHtml 来实现渲染
     */
    app.use(renderer.renderMiddleware);
    /**
     * 监听端口
     */
    app.listen(3000, () => console.log(`主访问地址： ${domain}:3000`));
};
