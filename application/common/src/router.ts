import { Router } from '@fmfe/genesis-app';
import Container from './container.vue';

import NProgress from 'nprogress'; // Progress 进度条
import 'nprogress/nprogress.css'; // Progress 进度条样式

export const createRouter = () => {
    const router = new Router({
        mode: 'history',
        routes: [
            {
                path: '/',
                redirect: '/home/'
            },
            {
                path: '/home/*',
                meta: {
                    ssrname: 'sub-home'
                },
                component: Container
            },
            {
                path: '/about/*',
                meta: {
                    ssrname: 'sub-about'
                },
                component: Container
            }
        ]
    });

    router.beforeEach((to, form, next) => {
        if ((process as any).browser) {
            NProgress.start();
        }

        next();

        if ((process as any).browser) {
            NProgress.done();
        }
    });

    return router;
};
