import Genesis, { Plugin } from '@fmfe/genesis-core';

export default class MyPlugin extends Plugin {
    // /**
    //  * 编译之前执行
    //  */
    // public beforeCompiler(type: Genesis.CompilerType) { }
    // /**
    //  * 修改 webpack 的配置
    //  */
    public chainWebpack({ config, target }) {
        if (target === 'client') {
            config.externals({
                // vue: 'Vue',
                // 'vue-router': 'VueRouter',
                'element-ui': 'ELEMENT'
                // axios: 'axios'
            });
        }
    }
    /**
     * 修改 babel 的配置
     */
    // public babel(config: Genesis.BabelConfig) {
    // }

    /**
     * 修改 postcss-loader 的配置
     */
    // public postcss(config: Genesis.PostcssOptions) {
    //     config.plugins.push({
    //         // 插件
    //     });
    // }

    /**
     * 编译完成之后执行
     */
    // public afterCompiler(type: Genesis.CompilerType) { }
    // /**
    //  * 渲染之前执行
    //  */
    public renderBefore(renderContext: Genesis.RenderContext) {
        renderContext.data.script +=
            // '<script src="https://cdn.bootcdn.net/ajax/libs/vue/' + require('vue').version + '/vue.min.js" defer></script>' +
            '<script src="https://cdn.bootcdn.net/ajax/libs/element-ui/' +
            require('element-ui').version +
            '/index.js" defer></script>' +
            '<script src="https://cdn.bootcdn.net/ajax/libs/babel-polyfill/7.12.1/polyfill.min.js"></script>';
        // '<script src="https://cdn.bootcdn.net/ajax/libs/axios/0.19.2/axios.min.js" defer></script>'
        // '<script src="https://cdn.jsdelivr.net/npm/axios@' + require('axios').version + '" defer></script>';
    }
    /**
     * 渲染之后执行
     */
    // public renderCompleted(renderContext: Genesis.RenderContext) {
    // }
}
